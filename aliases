# Utilities
alias l="ls -F"
alias myip='curl http://ipecho.net/plain; echo'


# KubeCtl Aliases
alias k="kubectl"
alias kpg="kubectl get pods | grep"
alias klf="kubectl logs -f"
alias kdp="kubectl describe pod"
alias kexec="kubectl exec"


# Docker

# Get latest container ID
alias dl="docker ps -l -q"

# Get container process
alias dps="docker ps"

# Get process included stop container
alias dpa="docker ps -a"

# Get images
alias di="docker images"

# Get container IP
alias dip="docker inspect --format '{{ .NetworkSettings.IPAddress }}'"

# Run deamonized container, e.g., $dkd base /bin/echo hello
alias dkd="docker run -d -P"

# Run interactive container, e.g., $dki base /bin/bash
alias dki="docker run -i -t -P"

# Execute interactive container, e.g., $dex base /bin/bash
alias dex="docker exec -i -t"

# Stop all containers
dstop() { docker stop $(docker ps -a -q); }

# Remove all containers
drm() { docker rm $(docker ps -a -q); }

# Stop and Remove all containers
alias drmf='docker stop $(docker ps -a -q) && docker rm $(docker ps -a -q)'

# Remove all images
dri() { docker rmi $(docker images -q); }

# Dockerfile build, e.g., $dbu tcnksm/test 
dbu() { docker build -t=$1 .; }

# Show all alias related docker
dalias() { alias | grep 'docker' | sed "s/^\([^=]*\)=\(.*\)/\1 => \2/"| sed "s/['|\']//g" | sort; }

# Bash into running container
dbash() { docker exec -it $(docker ps -aqf "name=$1") bash; }

# Docker-Compose Aliases
alias d-c="docker-compose"
alias dcu="docker-compose up"

alias f5="source ~/.zshrc"


# Git
alias gi="git init && gac 'Initial commit'"

alias gs="git status"
alias glog='git log --graph --all --decorate'
alias gac="git add . && git commit -m" # + commit message

alias gp="git push" # + remote & branch names
alias gpo="git push origin" # + branch name
alias gph="git push origin HEAD"
alias gpom="git push origin master"

alias gl="git pull" # + remote & branch names
alias gplo="git pull origin" # + branch name
alias gplom="git pull origin master"
alias gpr="hub pull-request"

alias fix-cs="make tools && make test-unit && git add . && git commit --amend && git push origin HEAD -f"

alias gb="git branch" # + branch name
alias gc="git checkout" # + branch name
alias gcb="git checkout -b" # + branch name

alias gclone="git clone" # + remote repository url
alias empty-commit="git commit --amend --no-edit && git push origin HEAD -f"


# Python server 
alias python-serve="python -m SimpleHTTPServer 8000"

# Pecl for PHP
# alias pecl='/usr/local/opt/php@7.3/bin/pecl'
alias pecl='/usr/local/bin/pecl'
alias composer="/usr/bin/php /usr/local/bin/composer.phar"
